import numpy as np
import scipy.sparse as sp
import scipy.linalg as la
import copy
import scipy.sparse.linalg
import scipy.sparse as sp
import pyamg
from funs_lr import *
import time

def jd(A, (u0, s0, v0), l0, m=2,
       dr=0, rmax=30, maxiter=10,
       maxiter_loc=10, restart=20,
       M=None, solve='jd', verb=1,
       dtype=np.float, l_exact=None,
       proj_res_tol=1e-6):

    n, r = u0.shape
    if verb >= 2:
        print 'Local system size:', 2*n*r+r**2
    U = [(u0, s0, v0)] # Basis
    log = {}
    log['res'] = []
    if l_exact:
        log['l_err'] = []
    (uk, sk, vk) = (u0, s0, v0)
    shift = copy.copy(l0)
    l = copy.copy(l0)
    V = [(uk, sk, vk)]
    ll = scal_prod_A((uk, sk, vk), A, (uk, sk, vk))
    resid = residual_lr(A, (uk, sk, vk), ll)
    log['res'].append(resid)
    log['rank'] = []
    log['time'] = [0]
    log['proj_res'] = [1.]
    if l_exact:
        log['l_err'].append(np.abs(ll - l_exact))

    if verb >= 1:
        print 'Iter num: {0:}, ||rk|| = {1:0.2e}'.format(0, resid)
        if l_exact:
            print '|l - lk| = {0:0.2e}'.format(log['l_err'][-1])


    if M:
        h = np.pi / np.sqrt(M['N'])
        tk = np.exp(h * np.arange(-M['N'], M['N']+1))
        ck = h * tk
        if M['shift'] == 'adaptive':
            raise Exception('No support so far')
        else:
            A0 = [[A[0][0], A[0][1]- M['shift']/2 * sp.eye(n)], [A[1][0]- M['shift']/2 * sp.eye(n), A[1][1]]]
        expA01 = []
        expA02 = []
        for i in range(2*M['N']+1):
            expA01.append(scipy.linalg.expm(-tk[i]*np.array(A0[1][0].todense())))
            expA02.append(scipy.linalg.expm(-tk[i]*np.array(A0[0][1].todense())))

    for k in range(maxiter):
        t1 = time.time()
        xkm1 = copy.deepcopy((uk, sk, vk))
        if m >= 2:
            if k != 0:
                Q = np.array(U).T
                Q, _ = np.linalg.qr(Q)
                U = []
                for i in range(len(Q[0, :])):
                    U.append(Q[:, i])
                UAU = np.zeros((len(U), len(U)), dtype=dtype)
                UU = np.zeros((len(U), len(U)), dtype=dtype)
                for i in range(len(U)):
                    for j in range(len(U)):
                        UAU[i, j] = U[i].dot(matvec_tang_A(U[j], solve='d'))
                        UU[i, j] = U[i].dot(U[j])

                UU = 0.5 * (UU + H(UU)).real
                l, c = la.eig(UAU, b=UU)
                l = l.real
                c = c.real
                ind = np.argmin(np.abs(l - shift))
                c = c[:, ind]
                l = l[ind]
                x_tang = 0.
                for i in range(len(U)):
                    x_tang += c[i] * U[i]
                u = x_tang[:n*r].reshape(n, r, order='f')
                v = x_tang[n*r:2*n*r].reshape(r, n, order='f').T
                s = x_tang[2*n*r:2*n*r+r**2].reshape(r, r, order='f')
                (uk, sk, vk) = sum_lr((uk, np.ones(r), v), (u + uk.dot(s), np.ones(r), vk))
                if dr == 0:
                    (uk, sk, vk) = round_lr((uk, sk, vk), 0, rmax=r)
                else:
                    if log['proj_res'][k] < 1e-6:
                        (uk, sk, vk) = round_lr((uk, sk, vk), 0, rmax=min(r+dr, rmax))
                    else:
                        (uk, sk, vk) = round_lr((uk, sk, vk), 0, rmax=r)
                sk = sk / np.linalg.norm(sk)

            r = len(sk)
        l = scal_prod_A((uk, sk, vk), A, (uk, sk, vk))
        resid = residual_lr(A, (uk, sk, vk), l)
        proj_resid = proj_residual_lr(A, (uk, sk, vk), l)
        log['rank'].append(r)
        log['res'].append(resid)
        log['proj_res'].append(proj_resid)
        if l_exact:
            log['l_err'].append(np.abs(l - l_exact))

        if verb >= 1:
            print 'Iter num: {0:}, ||rk|| = {1:0.2e}'.format(k, resid)
            if l_exact:
                print '|l - lk| = {0:0.2e}'.format(log['l_err'][-1])

        # Can be improved by adapting to accuracy
        if k > 5:
            shift = copy.copy(l)

        # Generating the matrix
        tang_A = [
            [get_vAv(A, uk, vk), get_vAu(A, uk, vk), get_vAuv(A, uk, vk)],
            [get_uAv(A, uk, vk), get_uAu(A, uk, vk), get_uAuv(A, uk, vk)],
            [get_uvAv(A, uk, vk), get_uvAu(A, uk, vk), get_uvAuv(A, uk, vk)]
        ]
        def matvec_tang_A(x, solve=solve):
            y = np.zeros((2*n*r+r**2, 1), dtype=dtype)
            x = x.reshape(-1, 1, order='f')
            u = matvec_Pu(uk, x[:n*r])
            v = matvec_Pv(vk, x[n*r: 2*n*r])
            if solve == 'jd':
                s = matvec_Ps(sk, x[2*n*r: 2*n*r+r**2])
            else:
                s = copy.copy(x[2*n*r: 2*n*r+r**2])
            y[:n*r] = my_dot(tang_A[0][0], u) + my_dot(tang_A[0][1], v) + my_dot(tang_A[0][2], s)
            y[n*r: 2*n*r] = my_dot(tang_A[1][0], u) + my_dot(tang_A[1][1], v) + my_dot(tang_A[1][2], s)
            y[2*n*r: 2*n*r+r**2] = my_dot(tang_A[2][0], u) + my_dot(tang_A[2][1], v) + my_dot(tang_A[2][2], s)
            y[:n*r] = matvec_Pu(uk, y[:n*r])
            y[n*r: 2*n*r] = matvec_Pv(vk, y[n*r: 2*n*r])
            if solve == 'jd':
                y[2*n*r: 2*n*r+r**2] = matvec_Ps(sk, y[2*n*r: 2*n*r+r**2])
            return y
        tang_A_linop = sp.linalg.LinearOperator((2*n*r+r**2, 2*n*r+r**2), matvec=lambda x: matvec_tang_A(x) - shift*x.reshape(-1, 1, order='f'))

        if M:
            if M['method'] == 'sinc':
                M1 = [[], []]
                M2 = [[], []]
                M3 = [[], []]
                for i in range(2*M['N']+1):
                    M1[0].append(ck[i]*scipy.linalg.expm(-tk[i]*vk.T.dot(A0[0][1].dot(vk.conj()))))
                    M1[1].append(expA01[i])
                    M2[0].append(expA02[i])
                    M2[1].append(ck[i]*scipy.linalg.expm(-tk[i]*H(uk).dot(A0[1][0].dot(uk))))
                    M3[0].append(scipy.linalg.expm(-tk[i]*vk.T.dot(A0[0][1].dot(vk.conj())))) # BUGGGGGGGGG
                    M3[1].append(ck[i]*scipy.linalg.expm(-tk[i]*H(uk).dot(A0[1][0].dot(uk))))
                M3 = full_mat(M3)
                M1B = [[], []]
                M2B = [[], []]
                for i in range(2*M['N']+1):
                    M1B[1].append(M1[1][i].dot(uk))
                    M1B[0].append(M1[0][i])
                    M2B[0].append(M2[0][i].dot(vk.conj()))
                    M2B[1].append(M2[1][i])
                vecsk = np.diag(sk).reshape(-1, 1, order='f')
                M3B = M3.dot(vecsk)
                BM1 = [[], []]
                BM2 = [[], []]
                for i in range(2*M['N']+1):
                    BM1[1].append(H(M1B[1][i]))
                    BM1[0].append(H(M1B[0][i]))
                    BM2[0].append(H(M2B[0][i]))
                    BM2[1].append(H(M2B[1][i]))
                BM3 = H(M3B)
                BM1B = 0.
                BM2B = 0.
                for i in range(2*M['N']+1):
                    BM1B += np.kron(M1B[0][i], H(uk).dot(M1B[1][i]))
                    BM2B += np.kron(vk.T.dot(M2B[0][i]), M2B[1][i])
                BM1B = np.linalg.inv(BM1B)
                BM2B = np.linalg.inv(BM2B)
                BM3B = 1./H(vecsk).dot(M3B)
                def prec_matvec(x):
                    x = x.reshape(-1, 1, order='f')
                    y = np.zeros((2*n*r+r**2, 1), dtype=dtype)

                    BMx = my_dot(BM1, x[:n*r])
                    u = BM1B.dot(BMx)
                    y[:n*r] = - my_dot(M1B, u) + my_dot(M1, x[:n*r])

                    BMx = my_dot(BM2, x[n*r: 2*n*r])
                    v = BM2B.dot(BMx)
                    y[n*r: 2*n*r] = - my_dot(M2B, v) + my_dot(M2, x[n*r: 2*n*r])
                    if solve == 'jd':
                        BMx = BM3.dot(x[2*n*r: 2*n*r+r**2])
                        s = BM3B * BMx
                        y[2*n*r: 2*n*r+r**2] = - M3B.dot(s) + M3.dot(x[2*n*r: 2*n*r+r**2])
                    else:
                        y[2*n*r: 2*n*r+r**2] = M3.dot(x[2*n*r: 2*n*r+r**2])
                    return y
            else:
                def prec_matvec(x):
                    y = np.zeros((2*n*r+r**2, 1), dtype=dtype)
                    x = x.reshape(-1, 1, order='f')
                    u = matvec_Pu(uk, x[:n*r])
                    v = matvec_Pv(vk, x[n*r: 2*n*r])
                    if solve == 'jd':
                        s = matvec_Ps(sk, x[2*n*r: 2*n*r+r**2])
                    else:
                        s = copy.copy(x[2*n*r: 2*n*r+r**2])
                    y[:n*r] = np.linalg.solve(full_mat(tang_A[0][0]), u)
                    y[n*r: 2*n*r] = np.linalg.solve(full_mat(tang_A[1][1]), v)
                    y[2*n*r: 2*n*r+r**2] = np.linalg.solve(full_mat(tang_A[2][2]), s)
                    y[:n*r] = matvec_Pu(uk, y[:n*r])
                    y[n*r: 2*n*r] = matvec_Pv(vk, y[n*r: 2*n*r])
                    if solve == 'jd':
                        y[2*n*r: 2*n*r+r**2] = matvec_Ps(sk, y[2*n*r: 2*n*r+r**2])
                    return y
            prec_lo = sp.linalg.LinearOperator((2*n*r+r**2, 2*n*r+r**2), matvec=prec_matvec)
        else:
            prec_lo = None

        # Generating rhs
        rhs = np.zeros((2*n*r+r**2, 1), dtype=dtype)
        if solve != 'inv':
            rhs[:n*r] = my_dot(tang_A[0][0], (uk.dot(np.diag(sk))).reshape(-1, 1, order='f'))
            rhs[n*r: 2*n*r] = my_dot(tang_A[1][1], (np.diag(sk).dot(H(vk))).reshape(-1, 1, order='f'))
            rhs[2*n*r: 2*n*r+r**2] = my_dot(tang_A[2][2], np.diag(sk).reshape(-1, 1, order='f')) - l * np.diag(sk).reshape(-1, 1, order='f')
            rhs[:n*r] = -matvec_Pu(uk, rhs[:n*r])
            rhs[n*r: 2*n*r] = -matvec_Pv(vk, rhs[n*r: 2*n*r])
            rhs[2*n*r: 2*n*r+r**2] = -rhs[2*n*r: 2*n*r+r**2]
        else:
            rhs[:n*r] = (uk.dot(np.diag(sk))).reshape(-1, 1, order='f')
            rhs[n*r: 2*n*r] = (np.diag(sk).dot(H(vk))).reshape(-1, 1, order='f')
            rhs[2*n*r: 2*n*r+r**2] = -np.diag(sk).reshape(-1, 1, order='f')
            rhs[:n*r] = -matvec_Pu(uk, rhs[:n*r])
            rhs[n*r: 2*n*r] = -matvec_Pv(vk, rhs[n*r: 2*n*r])

        r0 = np.linalg.norm(rhs[:, 0])
        t = sp.linalg.gmres(tang_A_linop, rhs/r0, x0=None, tol=(3./2)**(-(k+1)), maxiter=maxiter_loc, M=prec_lo, restart=restart)[0]
        if verb >= 2:
            print ''
            print 'Norm of res of loc syst', np.linalg.norm(tang_A_linop.dot(t) - rhs[:, 0])
        t = t.reshape(-1, 1, order='f')
        u = t[:n*r, :]
        v = t[n*r: 2*n*r, :]
        s = t[2*n*r: 2*n*r+r**2, :]

        u = matvec_Pu(uk, u).reshape(n, r, order='f')
        v = H(matvec_Pv(vk, v).reshape(r, n, order='f'))
        if solve == 'jd':
            s = matvec_Ps(sk, s).reshape(r, r, order='f')
        else:
            s = s.reshape(r, r, order='f')

        if verb >= 3:
            print 'Test ortho:', (np.linalg.norm(H(uk).dot(u)),
                                  np.linalg.norm(H(vk).dot(v)),
                                  np.linalg.norm(H(np.diag(sk).reshape(-1, 1, order='f')).dot(s.reshape(-1, 1, order='f'))))

        if m == 1:
            (u, s, v) = sum_lr((uk, np.ones(r), v), (u + uk.dot(s), np.ones(r), vk))
            (u, s, v) = round_lr((u, s, v), 0, rmax=2*r)
            s = s / np.linalg.norm(s)
            (u, s, v) = round_lr((u, s, v), 0, rmax=r)
            s = s / np.linalg.norm(s)
            (uk, sk, vk) = (u, s, v)

        if m >=2:
            if (k%(m-1) == 0):
                U = [np.hstack((0*uk.flatten(order='f'),
                                0*vk.T.flatten(order='f'),
                                np.diag(sk).flatten(order='f')))]
            if len(U) > 1:
                U = proj_tan_basis(U, (uk, sk, vk), xkm1)
            u_t = np.hstack((
                        u.flatten(order='f'),
                        v.T.flatten(order='f'),
                        s.flatten(order='f')
                    ))
            U.append(u_t)
            for i in range(len(U)):
                U[i] = U[i] / np.linalg.norm(U[i])
        if verb >= 3:
            print '(x, t)', np.abs(scal_prod((uk, sk, vk), (u, s, v)))
        if k > 0:
            log['time'].append(log['time'][k] + time.time() - t1)
        else:
            log['time'].append(time.time() - t1)

    return l, (uk, sk, vk), log
