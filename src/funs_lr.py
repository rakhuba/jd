import numpy as np
import matplotlib.pyplot as plt
import tt
import tt.cross
import scipy.sparse as sp
import scipy.linalg as la
import copy
import scipy.sparse.linalg
import scipy.sparse as sp
import pyamg

def H(A):
    return np.conjugate(A.T)


def proj_tan(x, xk):
    r = len(x[1])
    UkU = H(xk[0]).dot(x[0])
    VkV = H(xk[2]).dot(x[2])
    y0 = (xk[0].dot(UkU), x[1], x[2])
    y1 = (x[0]-xk[0].dot(UkU), x[1], xk[2].dot(VkV))
    y = sum_lr(y0, y1)
    return round_lr(y, 0)


def proj_tan_basis(U, x1, x0):
    b = len(U)
    n, r = x1[0].shape
    (u0, _, v0) = x0
    (u1, s1, v1) = x1
    s1_vec = np.diag(s1).reshape(-1, 1, order='f')
    U_proj = []
    for u in U:
        u_xi = u[: n*r].reshape(n, r, order='f')
        v_xi = (u[n*r: 2*n*r].reshape(r, n, order='f')).T
        s_xi = u[2*n*r: 2*n*r+r**2].reshape(r, r, order='f')
        u_eta = u0.dot(v_xi.T.dot(v1)) + u_xi.dot(v0.T.dot(v1)) + u0.dot(s_xi).dot(v0.T.dot(v1))
        v_eta = v0.dot(u_xi.T.dot(u1)) + v_xi.dot(u0.T.dot(u1)) + v0.dot(s_xi.T).dot(u0.T.dot(u1))
        s_eta = (u1.T.dot(u_xi)).dot(v0.T.dot(v1)) + (u1.T.dot(u0)).dot(v_xi.T.dot(v1)) + (u1.T.dot(u0)).dot(s_xi).dot(v0.T.dot(v1))
        #s_eta_vec = s_eta.reshape(-1, 1, order='f')
        u_eta = u_eta - u1.dot(u1.T.dot(u_eta))
        v_eta = v_eta - v1.dot(v1.T.dot(v_eta))
        #s_eta_vec = s_eta_vec - s_eta_vec.T.dot(s1_vec) * s1_vec
        #s_eta = s_eta_vec.reshape(r, r, order='f')
        u_proj = np.hstack((
            u_eta.flatten(order='f'),
            (v_eta.T).flatten(order='f'),
            s_eta.flatten(order='f')
        ))
        U_proj.append(u_proj)
    return U_proj



def proj_tan_full(U, xk, n, r):
    N, m = U.shape
    if n**2 != N:
        raise Exception('N != n**2')
    PU = 0 * U
    uk, sk, vk = np.linalg.svd(xk.reshape(n, n, order='f'))
    vk = H(vk)
    uk = uk[:, :r]
    vk = vk[:, :r]
    sk = sk[:r]
    for i in range(m):
        uv = U[:, i].reshape(n, n, order='f')
        uv1 = H(uk).dot(uv).dot(vk)
        uv1 = uk.dot(uv1).dot(H(vk))
        uv2 = (uv.dot(vk)).dot(H(vk))
        uv2 = uv2 - uk.dot((H(uk).dot(uv2)))
        uv3 = uk.dot((H(uk).dot(uv)))
        uv3 = uv3 - (uv3.dot(vk)).dot(H(vk))
        PU[:, i] = (uv1 + uv2 + uv3).reshape(-1, 1, order='f')[:, 0]
    return PU



    UkU = H(xk[0]).dot(x[0])
    VkV = H(xk[2]).dot(x[2])
    y0 = (xk[0].dot(UkU), x[1], x[2])
    y1 = (x[0]-xk[0].dot(UkU), x[1], xk[2].dot(VkV))
    y = sum_lr(y0, y1)
    return round_lr(y, 0)



def my_dot(A, x):
    R = len(A[0])
    n1, n2 = A[0][0].shape
    m1, m2 = A[1][0].shape
    x_shape = x.shape
    if len(x_shape) == 1:
        N = 1
    else:
        N = x_shape[1]
    M = len(x)
    y = np.zeros((n1*m1, N))
    for j in range(N):
        if len(x_shape) != 1:
            x_slice = x[:, j]
        else:
            x_slice = copy.copy(x)
        res = 0.0
        for i in range(R):
            A1 = A[0][i]
            A2 = A[1][i]
            X = x_slice.reshape(m2, n2, order='f')
            if m2 >= n2:
                X = A2.dot(X)
                X = (A1.dot(X.T)).T
            else:
                X = (A1.dot(X.T)).T
                X = A2.dot(X)
            X = (X).reshape(-1, 1, order='f')
            res = res + X
        y[:, j] = copy.copy(res[:, 0])
    return y

def full_mat(A):
    R = len(A[0])
    res = 0.0
    if scipy.sparse.issparse(A[0][0]):
        for i in range(R):
            res += np.kron(np.array(A[0][i].todense()), A[1][i])
    elif scipy.sparse.issparse(A[1][0]):
        for i in range(R):
            res += np.kron(A[0][i], np.array(A[1][i].todense()))
    else:
        for i in range(R):
            res += np.kron(A[0][i], A[1][i])
    return res

def scal_prod_A((u1, s1, v1), A, (u2, s2, v2)):
    uvAuv = [[], []]
    R = len(A[0])
    for i in range(R):
        vAv = v1.T.dot(A[0][i].dot(v2.conj()))
        uAu = H(u1).dot(A[1][i].dot(u2))
        uvAuv[0].append(vAv)
        uvAuv[1].append(uAu)
    S1 = np.diag(s1).reshape(-1, 1, order='f')
    S2 = np.diag(s2).reshape(-1, 1, order='f')
    return H(S1).dot(my_dot(uvAuv, S2))[0][0]

# x1 = (u0.dot(np.diag(s0)).dot(H(v0))).reshape(-1, 1, order='f')
# x2 = (u.dot(np.diag(s)).dot(H(v))).reshape(-1, 1, order='f')
# print (scal_prod_A((u0, s0, v0), A, (u, s, v)) - H(x1).dot(A_full.dot(x2))[0][0]) /  H(x1).dot(A_full.dot(x2))[0][0]

def scal_prod((u1, s1, v1), (u2, s2, v2)):
    u1_new = u1.dot(np.diag(s1))
    u2_new = u2.dot(np.diag(s2))
    Ua = H(u1_new).dot(u2_new)
    Ub = H(v1).dot(v2)
    return np.sum(Ua*Ub)

# x1 = (u0.dot(np.diag(s0)).dot(H(v0))).reshape(-1, 1, order='f')
# x2 = (u.dot(np.diag(s)).dot(H(v))).reshape(-1, 1, order='f')
# print (scal_prod((u0, s0, v0), (u, s, v)) - H(x1).dot(x2)[0][0]) /  H(x1).dot(A_full.dot(x2))[0][0]

def sum_lr((u1, s1, v1), (u2, s2, v2)):
    if u1 is not None:
        u = np.concatenate((u1, u2), axis=1)
        s = np.concatenate((s1, s2), axis=0)
        v = np.concatenate((v1, v2), axis=1)
        return (u, s, v)
    else:
        return (u2, s2, v2)

def round_lr((u, s, v), eps, rmax=None):
    qu, ru = np.linalg.qr(u)
    qv, rv = np.linalg.qr(v)
    G = ru.dot(np.diag(s)).dot(H(rv))
    ur, sr, vr = np.linalg.svd(G, full_matrices=False)
    if sr[0] != 0.:
        r = np.argmax(sr/sr[0] < eps)
    else:
        r = 1
    if r == 0:
        r = len(s)
    if rmax is not None:
        if r > rmax:
            r = copy.copy(rmax)
    ur = ur[:, :r]
    vr = H(vr[:r, :])
    sr = sr[:r]
    ur = qu.dot(ur)
    vr = qv.dot(vr)
    return (ur, sr, vr)

# x = (u0.dot(np.diag(s0)).dot(H(v0))).reshape(-1, 1, order='f')
# (ur, sr, vr) = round_lr(sum_lr((u0, s0, v0), (u0, s0, v0)), 1e-12, rmax=3)
# xr = (ur.dot(np.diag(sr)).dot(H(vr))).reshape(-1, 1, order='f')
# print np.linalg.norm(2*x - xr)

def residual_lr(A, (u, s, v), l):
    Auv = [[], []]
    R = len(A[0])
    Ax = (None, None, None)
    for i in range(R):
        Av = (A[0][i].dot(v.conj())).conj()
        Au = A[1][i].dot(u)
        Ax = sum_lr(Ax, (Au, s, Av))
        Ax = round_lr(Ax, 1e-14, rmax=None)
    residual = sum_lr(Ax, (u, -l*s, v))
    (_, res, _) = round_lr(residual, 1e-14, rmax=None)
    return np.sqrt((res**2).sum())

def proj_residual_lr(A, (u, s, v), l):
    Auv = [[], []]
    R = len(A[0])
    Ax = (None, None, None)
    for i in range(R):
        Av = (A[0][i].dot(v.conj())).conj()
        Au = A[1][i].dot(u)
        Ax = sum_lr(Ax, (Au, s, Av))
        Ax = round_lr(Ax, 1e-14, rmax=None)
    residual = sum_lr(Ax, (u, -l*s, v))
    residual = proj_tan(residual, (u, s, v))
    (_, res, _) = round_lr(residual, 1e-14, rmax=None)
    return np.sqrt((res**2).sum())


#residual_lr(A, (u0, s0, v0), l0)


# Auxiliary funcs

def get_vAv(A, u, v):
    R = len(A[0])
    A_block = [[], []]
    for i in range(R):
        vAv = v.T.dot(A[0][i].dot(v.conj()))
        A_block[0].append(vAv)
        A_block[1].append(A[1][i])
    return A_block

def get_vAu(A, u, v):
    R = len(A[0])
    A_block = [[], []]
    for i in range(R):
        vA = (A[0][i].T.dot(v)).T
        Au = A[1][i].dot(u)
        A_block[0].append(vA)
        A_block[1].append(Au)
    return A_block

def get_vAuv(A, u, v):
    R = len(A[0])
    A_block = [[], []]
    for i in range(R):
        vAv = v.T.dot(A[0][i].dot(v.conj()))
        Au = A[1][i].dot(u)
        A_block[0].append(vAv)
        A_block[1].append(Au)
    return A_block



def get_uAv(A, u, v):
    R = len(A[0])
    A_block = [[], []]
    for i in range(R):
        uA = (A[1][i].T.dot(u.conj())).T
        Av = A[0][i].dot(v.conj())
        A_block[0].append(Av)
        A_block[1].append(uA)
    return A_block

def get_uAu(A, u, v):
    R = len(A[0])
    A_block = [[], []]
    for i in range(R):
        uAu = H(u).dot(A[1][i].dot(u))
        A_block[0].append(A[0][i])
        A_block[1].append(uAu)
    return A_block

def get_uAuv(A, u, v):
    R = len(A[0])
    A_block = [[], []]
    for i in range(R):
        uAu = H(u).dot(A[1][i].dot(u))
        Av = A[0][i].dot(v.conj())
        A_block[0].append(Av)
        A_block[1].append(uAu)
    return A_block



def get_uvAv(A, u, v):
    R = len(A[0])
    A_block = [[], []]
    for i in range(R):
        uA = (A[1][i].T.dot(u.conj())).T
        vAv = v.T.dot(A[0][i].dot(v.conj()))
        A_block[0].append(vAv)
        A_block[1].append(uA)
    return A_block

def get_uvAu(A, u, v):
    R = len(A[0])
    A_block = [[], []]
    for i in range(R):
        uAu = H(u).dot(A[1][i].dot(u))
        vA = (A[0][i].T.dot(v)).T
        A_block[0].append(vA)
        A_block[1].append(uAu)
    return A_block

def get_uvAuv(A, u, v):
    R = len(A[0])
    A_block = [[], []]
    for i in range(R):
        uAu = H(u).dot(A[1][i].dot(u))
        vAv = v.T.dot(A[0][i].dot(v.conj()))
        A_block[0].append(vAv)
        A_block[1].append(uAu)
    return A_block


def matvec_Pu(u, x):
    n, r = u.shape
    X = x.reshape(n, r, order='f')
    X = u.dot(H(u).dot(X))
    return x - X.reshape(-1, 1, order='f')

def matvec_Pv(v, x):
    n, r = v.shape
    #r
#     Ir = np.eye(r)
#     Pv = np.kron(v, Ir)
#     return x - Pv.dot(H(Pv).dot(x))

    X = x.reshape(r, n, order='f')
    X = (X.dot(v)).dot(H(v))
    return x - X.reshape(-1, 1, order='f')

def matvec_Ps(s, x):
    r = len(s)
    S = np.diag(s).reshape(-1, 1, order='f')
    return x - S * (H(S).dot(x))


def inv_iter(A, x0, lam0, tol=1e-6, maxiter=10, restart_loc=20, maxiter_loc=10):
    x = copy.copy(x0)[:, 0]# / np.linalg.norm(x0)
    lam = copy.copy(lam0)
    #n = len(x)
    #print 'l0', H(x).dot(my_dot(A, x))
    for i in range(maxiter):
        A_linop = sp.linalg.LinearOperator((n*r, n*r), matvec=lambda x: (my_dot(A, x)[:, 0] - lam * x))
        x, info = pyamg.krylov.fgmres(A_linop, x, x0=x, tol=1e-16, restrt=restart_loc, maxiter=maxiter_loc)
        x /= np.linalg.norm(x)
        lam = H(x).dot(my_dot(A, x))
    print np.linalg.norm(my_dot(A, x)[:, 0] - lam*x)
    return x, lam
